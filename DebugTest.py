#encode=utf8
'''
Created on 20231030
@autor: xing
@note:
'''
print('This is a summary processing.')
iMax = eval(input('A positive number:'))

print('--1.1:1+2+...+9+10<梯形公式--------')
iSum = (1+iMax)*(iMax//2)
if((iMax%2)==1):
    iSum+=iMax-(iMax//2)
print(iSum)

print('--1.2: 1+2+...+9+10 ---------------')
i=1
iSum=0
while(i<=iMax):
    iSum+=i
    i+=1
print(iSum)
print('--2.1: 1+3+5+7+9 ---------')
i=1
iSum=0
while(i<=iMax):
    if((i%2)==0):
        i+=1
        continue
    iSum+=i
    i+=1
print(iSum)